## Mancala
**Board Setup**
    - Each of the two players has his six pits in front of him. To the right of the six pits, each player has a larger pit. At the start of the game, there are six stones in each of the six round pits .

**Rules**

1. **Game Play** - 
The player who begins with the first move picks up all the stones in any of his own six pits, and sows the stones on to the right, one in each of the following pits, including his own big pit. No stones are put in the opponents' big pit. If the player's last stone lands in his own big pit, he gets another turn. This can be repeated
several times before it's the other player's turn.

2. **Capturing Stones** - 
During the game the pits are emptied on both sides. Always when the last stone lands in an own empty pit, the player captures his own stone and all stones in the opposite pit (the other player’s pit) and puts them in his own (big or little?) pit.

3. **The Game Ends** - 
The game is over as soon as one of the sides runs out of stones. The player who still has stones in his pits keeps them and puts them in his big pit. The winner of the game is the player who has the most stones in his big pit. You can also find some visual explanations of the game rules by running a
Google Search for Mancala or Kalaha game.

## Pre-Assumption
- Both the user will play game in same tab (because of this socket programming not added)
- When game initialize both the player will added.
- Product is deploy with single instance (caching and lock mechanism work with single instance)
- Pits always be higher then stone (Game logic is supporting any condition but test case are not supporting right now only that condition)

## Tool
- Java 8
- Maven
- SpringBoot

**Notes** : After installation of maven and java8 make sure you have set env. variable for maven and java.

## Steps to Installation 
1. cd <any_folder>
2. git clone https://gitlab.com/java-techie/mancala.git
3. Go to inside mancala folder (project directory)
4. mvn clean install
5. java -jar ./target/mancala-2021.11.16.RC01.jar
6. Game will be start by using link http://localhost:8080/mancala/
7. All API details will available in swagger - http://localhost:8080/mancala/swagger-ui.html#/MancalaGame_API

## Roadmap for Future Enhancement 
    
1. Redis Caching and Distributed Locks **(NFR)**
    - Utilizes Redis for caching game objects.
    - Implements distributed locks with the game ID as the key to handle concurrent user accessibility and manage requests across multiple servers. 
    - add TTL caching logic base on session expiration time so that unwanted data will not be available in cache which saved memory.
2. Sonar/Jacoco Code Coverage Integration **(NFR)**
3. PAUSE/RESUME game
    - Implementation of pause/resume functionality with object state storage.
4. Multiplayer Support
    - Enables two players to play against each other using socket connections in different browsers.
    - should allow n number player to play game (what are the different rule that we need to Implementation , small change in enity desing and some more API)
5. CPU Opponent
    - Implements logic for playing against a computer opponent with selectable difficulty levels: Easy, Medium, and Hard.
6. Audit Management
    - Game move audit & tracking to replay and analysis for future versions of Computer based moves.
7. Reward Points 
    - We can add logic for earning points base on winning the game. 
8. Player managment.
9. User Authentication
    - Implements a robust user authentication system to enhance security and personalize the gaming experience.

## Contribution Guidelines
We welcome contributions to enhance the Mancala game project. Follow these guidelines:

Fork the repository and create a new branch.
Make your changes and test thoroughly.
Submit a pull request with a detailed description of your changes.
Let's create an amazing Mancala gaming experience together! 🎮

## License
For open source projects, say how it is licensed.
