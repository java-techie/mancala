package com.bol.mancala.model;

import com.bol.mancala.constants.ApplicationContants;
import com.bol.mancala.enums.GameWinner;
import com.bol.mancala.enums.PlayerTurn;
import com.bol.mancala.utils.Utils;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
@Slf4j
public class Board {

	private Player firstPlayer;
	private Player secondPlayer;
	private PlayerTurn playerTurn;
	private GameWinner result;

	public Board(String firstPlayerId, String secondPlayerId) {
		this.firstPlayer = new Player(firstPlayerId);
		this.secondPlayer = new Player(secondPlayerId);
		this.playerTurn = PlayerTurn.FIRST_PLAYER;
	}

	public void makeMove(int pitIndex) {
		if (pitIndex >= ApplicationContants.INITIALLY_PITS) {
			throw new IllegalArgumentException("Wrong pit index: " + pitIndex + ", pit index should be from 0 to 5");
		}

		boolean repeatTurn;
		log.info("{} turn.", playerTurn);
		if (playerTurn == PlayerTurn.FIRST_PLAYER) {
			repeatTurn = makeMove(pitIndex, firstPlayer, secondPlayer);
		} else {
			repeatTurn = makeMove(pitIndex, secondPlayer, firstPlayer);
		}
		if (!repeatTurn) {
			this.playerTurn = Utils.playerTurnChange(playerTurn);
		}
		checkFinished();
	}

	/**
	 * Make move from pit index in current player's half of board.
	 *
	 * @return true, if current player get extra turn, otherwise false
	 */
	private boolean makeMove(int pitIndex, Player myHalf, Player opponentHalf) {
		Integer stones = myHalf.set(pitIndex, 0);
		if (stones == 0) {
			throw new IllegalArgumentException("Can't make move as pit with index: " + pitIndex + " is empty");
		}

		int startIndex = pitIndex + 1;
		while (stones > 0) { // Iterate till stones becomes 0
			stones = myHalf.addingInMyPits(startIndex, stones, opponentHalf);
			startIndex = 0; // reset index for next interation

			if (stones > 0) {
				stones = myHalf.addScore(stones);
				if (stones == 0) {
					// keep same player turn as last pit is blank
					return true;
				}
			}

			//
			stones = opponentHalf.addingInOpponentPits(stones);
		}

		return false;
	}

	private void checkFinished() {
		int firstPlayerHalfSum = firstPlayer.getSum();
		int secondPlayerHalfSum = secondPlayer.getSum();
		if (firstPlayerHalfSum == 0 || secondPlayerHalfSum == 0) {
			firstPlayer.moveAllToLargePit();
			secondPlayer.moveAllToLargePit();

			if (firstPlayer.getScore() > secondPlayer.getScore()) {
				this.result = GameWinner.FIRST_PLAYER;
			} else if (secondPlayer.getScore() > firstPlayer.getScore()) {
				this.result = GameWinner.SECOND_PLAYER;
			} else {
				this.result = GameWinner.DRAW;
			}
		}
	}

	public boolean isGameOver() {
		return result != null;
	}
}
