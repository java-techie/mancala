package com.bol.mancala.model;

import com.bol.mancala.enums.GameStatus;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class MancalaGame {

	private String id;
	private GameStatus status;
	private Board board;
	private long createdAt;
	private long updatedAt;
}