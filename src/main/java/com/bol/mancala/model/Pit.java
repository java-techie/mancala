package com.bol.mancala.model;

import java.util.List;
import java.util.stream.Collectors;

import com.bol.mancala.dto.PitDto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Pit {
	private int stone;

	public static PitDto fromBoard(Pit pit) {
		return new PitDto(pit.getStone());
	}

	public static List<PitDto> fromBoard(List<Pit> pits) {
		return pits.stream().map(Pit::fromBoard).collect(Collectors.toList());
	}
}
