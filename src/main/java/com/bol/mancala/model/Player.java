package com.bol.mancala.model;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.bol.mancala.constants.ApplicationContants;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class Player {

	private String playerId;
	private List<Pit> pits;
	private int score;

	public Player(String playerId) {
		this.score = 0; // initially score set to 0
		this.playerId = playerId;
		pits = new ArrayList<>();
		for (int i = 0; i < ApplicationContants.INITIALLY_PITS; i++) {
			pits.add(new Pit(ApplicationContants.INITIALLY_STONE_IN_ONE_PITS));
		}
	}

	public int getSum() {
		return pits.stream().mapToInt(e -> e.getStone()).sum();
	}

	public Integer set(int index, Integer value) {
		int oldValue = pits.get(index).getStone();
		pits.get(index).setStone(value);
		return oldValue;
	}

	public Integer addingInMyPits(Integer index, Integer stones, Player oppositeHalf) {
		ListIterator<Pit> iterator = pits.listIterator(index);
		ListIterator<Pit> oppositeIterator = oppositeHalf.pits.listIterator(ApplicationContants.INITIALLY_PITS - index);
		while (stones > 0 && iterator.hasNext()) {
			Pit currentPit = iterator.next();
			Pit oppositePit = oppositeIterator.previous();
			if (currentPit.getStone() == 0 && stones == 1 && oppositePit.getStone() > 0) {
				score += stones + oppositePit.getStone();
				oppositePit.setStone(0);
			} else {
				currentPit.setStone(currentPit.getStone() + 1);
			}
			stones--;
		}
		return stones;
	}

	public Integer addingInOpponentPits(Integer stones) {
		ListIterator<Pit> iterator = pits.listIterator();
		while (stones > 0 && iterator.hasNext()) {
			Pit pit = iterator.next();
			pit.setStone(pit.getStone() + 1);
			stones--;
		}
		return stones;
	}

	public Integer addScore(Integer stones) {
		score += 1;
		return --stones;
	}

	public void moveAllToLargePit() {
		for (int i = 0; i < pits.size(); i++) {
			score += pits.get(i).getStone();
			pits.get(i).setStone(0);
		}
	}

}
