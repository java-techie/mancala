package com.bol.mancala.aop;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.bol.mancala.constants.ApplicationContants;
import com.bol.mancala.dto.ErrorDto;
import com.bol.mancala.exception.ForbiddenException;
import com.bol.mancala.exception.GameLogicException;
import com.bol.mancala.exception.GameNotFoundException;

import lombok.extern.slf4j.Slf4j;

/**
 * This advice is used to for application global exception handling TODO : HTML
 * page not created for exception which should be as per different schenario.
 * 
 * @author anghan
 *
 */
@ControllerAdvice
@Slf4j
public class GlobalControllerAdvice {

	@ExceptionHandler(GameNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	public ModelAndView handleNotFoundError(Exception ex) {
		ModelAndView mav = new ModelAndView(ApplicationContants.ERROR_VIEW);
		mav.addObject(ApplicationContants.MESSAGE, ex.getMessage());
		mav.addObject(ApplicationContants.STATUS, HttpStatus.NOT_FOUND);
		return mav;
	}

	@ExceptionHandler(ForbiddenException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public ModelAndView handleForbiddenError(Exception ex) {
		ModelAndView mav = new ModelAndView(ApplicationContants.ERROR_VIEW);
		mav.addObject(ApplicationContants.MESSAGE, ex.getMessage());
		mav.addObject(ApplicationContants.STATUS, HttpStatus.FORBIDDEN);
		return mav;
	}

	@ExceptionHandler({ GameLogicException.class, IllegalArgumentException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ErrorDto handleLogicError(HttpServletRequest req, Exception ex) {
		return new ErrorDto("ERROR_CODE", ex.getMessage());
	}

	@ExceptionHandler(Exception.class)
	public ModelAndView handleError(HttpServletRequest req, Exception ex) {
		ModelAndView mav = new ModelAndView(ApplicationContants.ERROR_VIEW);
		mav.addObject(ApplicationContants.MESSAGE, ex.getMessage());
		mav.addObject(ApplicationContants.STATUS, HttpStatus.INTERNAL_SERVER_ERROR);
		return mav;
	}
}
