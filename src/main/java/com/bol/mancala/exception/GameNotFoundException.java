package com.bol.mancala.exception;

public class GameNotFoundException extends RuntimeException {

	public GameNotFoundException(String uuid) {
		super("Game with UUID=" + uuid + " doesn't exist");
	}
}
