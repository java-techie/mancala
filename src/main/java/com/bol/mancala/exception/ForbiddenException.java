package com.bol.mancala.exception;

public class ForbiddenException extends RuntimeException {

	public ForbiddenException(String playerId, String gameId) {
		super("Player with ID: " + playerId + " is not part of this game with id: " + gameId);
	}
}