package com.bol.mancala.controller;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.slf4j.MDC;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.bol.mancala.constants.ApplicationContants;
import com.bol.mancala.constants.ErrorCodeMessages;
import com.bol.mancala.dto.ErrorDto;
import com.bol.mancala.dto.GameDto;
import com.bol.mancala.service.GameService;
import com.bol.mancala.utils.Utils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import io.swagger.annotations.ExampleProperty;

@Api(tags = "MancalaGame API")
@Controller
@RequestMapping("/game")
public class GameController {

	private final GameService gameService;

	public GameController(GameService gameService) {
		this.gameService = gameService;
	}

	public ModelAndView index() {
		ModelAndView modelAndView = new ModelAndView();
		modelAndView.setViewName("index");
		return modelAndView;
	}

	@PostMapping("/create")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", examples = @Example(value = @ExampleProperty(mediaType = "application/json", value = com.bol.mancala.constants.SwaggerConstants.CREATE_GAME_RESPONSE_EXAMPLE))) })
	public ResponseEntity<GameDto> create() {
		Utils.generateTraceId();
		GameDto gameDto = gameService.createGame();
		return ResponseEntity.status(HttpStatus.OK)
				.header(ApplicationContants.PARAM_TRACE_ID, MDC.get(ApplicationContants.PARAM_TRACE_ID)).body(gameDto);
	}

	@PutMapping("{gameId}/reset")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", examples = @Example(value = @ExampleProperty(mediaType = "application/json", value = com.bol.mancala.constants.SwaggerConstants.RESET_GAME_RESPONSE_EXAMPLE))) })
	public ResponseEntity<GameDto> reset(@PathVariable("gameId") String gameId) {
		Utils.generateTraceId();
		GameDto gameDto = gameService.resetGame(gameId);
		return ResponseEntity.status(HttpStatus.OK)
				.header(ApplicationContants.PARAM_TRACE_ID, MDC.get(ApplicationContants.PARAM_TRACE_ID)).body(gameDto);
	}

	@DeleteMapping("{gameId}")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", examples = @Example(value = @ExampleProperty(mediaType = "application/json", value = com.bol.mancala.constants.SwaggerConstants.DELETE_GAME_RESPONSE_EXAMPLE))) })
	public ResponseEntity<Object> delete(@PathVariable("gameId") String gameId) {
		Utils.generateTraceId();
		if (gameService.deleteGame(gameId)) {
			return ResponseEntity.status(HttpStatus.OK)
					.header(ApplicationContants.PARAM_TRACE_ID, MDC.get(ApplicationContants.PARAM_TRACE_ID))
					.body("deleted successfully.");
		} else {
			// 204 as no content found for that game id
			return ResponseEntity.status(HttpStatus.NO_CONTENT)
					.header(ApplicationContants.PARAM_TRACE_ID, MDC.get(ApplicationContants.PARAM_TRACE_ID))
					.body(new ErrorDto(ErrorCodeMessages.MANCALA_GAME_NOT_ABLE_DELETE.getErrorCode(),
							ErrorCodeMessages.MANCALA_GAME_NOT_ABLE_DELETE.getErrorMessage()));
		}
	}

	@GetMapping("/{gameId}")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", examples = @Example(value = @ExampleProperty(mediaType = "application/json", value = com.bol.mancala.constants.SwaggerConstants.GET_GAME_RESPONSE_EXAMPLE))) })
	public ResponseEntity<GameDto> getGame(@PathVariable("gameId") String gameId) {
		Utils.generateTraceId();
		GameDto dto = gameService.loadGame(gameId);
		return ResponseEntity.status(HttpStatus.OK)
				.header(ApplicationContants.PARAM_TRACE_ID, MDC.get(ApplicationContants.PARAM_TRACE_ID)).body(dto);
	}

	@PostMapping("/{gameId}/makeMove")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "OK", examples = @Example(value = @ExampleProperty(mediaType = "application/json", value = com.bol.mancala.constants.SwaggerConstants.MAKE_MOVE_RESPONSE_EXAMPLE))) })
	public ResponseEntity<GameDto> makeMove(@PathVariable("gameId") String gameId,
			@RequestParam @Valid @Min(0) @Max(ApplicationContants.INITIALLY_PITS - 1) Integer pitIndex,
			String playerId) {
		Utils.generateTraceId();
		gameService.makeMove(gameId, playerId, pitIndex);
		GameDto game = gameService.loadGame(gameId);
		return ResponseEntity.status(HttpStatus.OK)
				.header(ApplicationContants.PARAM_TRACE_ID, MDC.get(ApplicationContants.PARAM_TRACE_ID)).body(game);
	}

	// PAUSE/RESUME Game - change status game (PUT method)
}
