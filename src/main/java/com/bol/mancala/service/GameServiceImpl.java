package com.bol.mancala.service;

import java.util.Objects;
import java.util.UUID;

import org.springframework.stereotype.Service;

import com.bol.mancala.dto.GameDto;
import com.bol.mancala.enums.GameStatus;
import com.bol.mancala.enums.PlayerTurn;
import com.bol.mancala.exception.ForbiddenException;
import com.bol.mancala.exception.GameLogicException;
import com.bol.mancala.exception.GameNotFoundException;
import com.bol.mancala.model.Board;
import com.bol.mancala.model.MancalaGame;
import com.bol.mancala.utils.ExclusiveLockByKey;
import com.bol.mancala.utils.KeyValueCache;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GameServiceImpl implements GameService {

	private static final KeyValueCache<String, MancalaGame> gameCache = new KeyValueCache<>();

	private static final ExclusiveLockByKey<String> lock = new ExclusiveLockByKey<String>();

	@Override
	public GameDto createGame() {
		MancalaGame game = new MancalaGame().setStatus(GameStatus.IN_PROGRESS)
				// .setFirstPlayerId(UUID.randomUUID().toString()).setSecondPlayerId(UUID.randomUUID().toString())
				.setBoard(new Board(UUID.randomUUID().toString(), UUID.randomUUID().toString()))
				.setCreatedAt(System.currentTimeMillis()).setUpdatedAt(System.currentTimeMillis());
		String uuid = UUID.randomUUID().toString();
		game.setId(uuid);
		gameCache.put(uuid, game);
		log.info("game created and added in DB gameId= {}", uuid);
		return GameDto.fromMancalaGame(game);
	}

	@Override
	public GameDto resetGame(String gameId) {
		MancalaGame game = validateGame(gameId);
		MancalaGame resetGame = new MancalaGame().setStatus(GameStatus.IN_PROGRESS)
				.setBoard(new Board(game.getBoard().getFirstPlayer().getPlayerId(),
						game.getBoard().getSecondPlayer().getPlayerId()))
				.setUpdatedAt(System.currentTimeMillis());
		resetGame.setId(game.getId());
		gameCache.put(gameId, resetGame);
		log.info("game reset successfully and updated in DB gameId= {}", gameId);
		return GameDto.fromMancalaGame(resetGame);
	}

	@Override
	public boolean deleteGame(String gameId) {
		return gameCache.removeElement(gameId) != null;
	}

	@Override
	public GameDto loadGame(String gameId) {
		MancalaGame game = validateGame(gameId);
		log.info("game loaded successfully with gameId= {}", game.getId());
		return GameDto.fromMancalaGame(game);
	}

	@Override
	public void makeMove(String gameId, String playerId, int pitIndex) {
		if (lock.tryLock(gameId)) {
			try {
				log.info("makeMove for gameId = {} , playerId= {} , pitIndex= {}", gameId, playerId, pitIndex);

				// Validation
				MancalaGame game = validateGame(gameId);
				validatePlayer(playerId, game);
				log.info("Game and player validated successfully...!");

				// Logic
				Board board = game.getBoard();
				board.makeMove(pitIndex);
				game.setBoard(board);
				log.info("Game movement done...!");

				// Game Over Check
				if (board.isGameOver()) {
					game.setStatus(GameStatus.FINISHED);
					log.info("It's Game Over and winner for game = {} is player = {}", gameId,
							board.getResult().toString());
				}
				game.setUpdatedAt(System.currentTimeMillis());
				gameCache.put(gameId, game);
			} finally {
				lock.unlock(gameId);
			}
		} else {
			log.info("Do not allow concurrent request for same gameId = {} , playerId= {}", gameId, playerId);
		}

	}

	private void validatePlayer(String playerId, MancalaGame game) {
		if (!Objects.equals(game.getBoard().getFirstPlayer().getPlayerId(), playerId)
				&& !Objects.equals(game.getBoard().getSecondPlayer().getPlayerId(), playerId)) {
			throw new ForbiddenException(playerId, game.getId());
		}
		Board board = game.getBoard();
		PlayerTurn playerTurn = game.getBoard().getFirstPlayer().getPlayerId().equals(playerId)
				? PlayerTurn.FIRST_PLAYER
				: PlayerTurn.SECOND_PLAYER;
		if (board.getPlayerTurn() != playerTurn) {
			throw new GameLogicException(
					"Can't make move, it's not your turn. Next player is: " + board.getPlayerTurn());
		}
		log.info("playerId= {} is valid player for gameId = {}", playerId, game.getId());
	}

	private MancalaGame validateGame(String gameId) {
		MancalaGame game = gameCache.get(gameId);
		if (game == null) {
			throw new GameNotFoundException(gameId);
		}
		log.info("gameId= {} is valid.", game.getId());
		return game;
	}
}
