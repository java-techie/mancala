package com.bol.mancala.service;

import com.bol.mancala.dto.GameDto;
import com.bol.mancala.exception.GameNotFoundException;

/**
 * Class is used to all Game operation.
 * 
 * @author anghan
 *
 */
public interface GameService {

	/**
	 * Method is used to create game first time which will initialize 2 player and
	 * game.
	 * 
	 * @return
	 */
	GameDto createGame();

	/**
	 * Method is used to load game by id.
	 * 
	 * @param gameId
	 * @return
	 */
	GameDto loadGame(String gameId);

	/**
	 * Method is used to make move of player base on pitInde. it will throw
	 * exception base on different scenario if game not valid then
	 * GameNotFoundException , if not valid player then ForbiddenException, if not
	 * valid turn or not valid status of game then GameLogicException
	 * 
	 * @throws GameNotFoundException, ForbiddenException, GameLogicException
	 * @param gameId
	 * @param playerId
	 * @param pitIndex
	 */
	void makeMove(String gameId, String playerId, int pitIndex);

	/**
	 * Method is used to reset the same game base on gameId - all stone will reset
	 * to all pits it's like oyu starting same game again.
	 * 
	 * @param gameId
	 * @return
	 */
	GameDto resetGame(String gameId);

	/**
	 * Method is used to delete game from DB so that it won't available.
	 * 
	 * @param gameId
	 * @return
	 */
	boolean deleteGame(String gameId);
}
