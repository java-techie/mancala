package com.bol.mancala.dto;

import java.util.List;

import com.bol.mancala.enums.GameWinner;
import com.bol.mancala.enums.PlayerTurn;
import com.bol.mancala.model.Board;
import com.bol.mancala.model.Pit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class BoardDto {

	private List<PitDto> firstPlayerPits;
	private int firstPlayerScore;
	private String firstPlayerId;
	private List<PitDto> secondPlayerPits;
	private int secondPlayerScore;
	private String secondPlayerId;
	private PlayerTurn playerTurn = PlayerTurn.FIRST_PLAYER;
	private GameWinner result;

	public static BoardDto fromBoard(Board board) {
		return new BoardDto(Pit.fromBoard(board.getFirstPlayer().getPits()), board.getFirstPlayer().getScore(),
				board.getFirstPlayer().getPlayerId(), Pit.fromBoard(board.getSecondPlayer().getPits()),
				board.getSecondPlayer().getScore(), board.getSecondPlayer().getPlayerId(), board.getPlayerTurn(),
				board.getResult());
	}
}
