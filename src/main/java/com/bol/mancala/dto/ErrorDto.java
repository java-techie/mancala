package com.bol.mancala.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ErrorDto {

	public final String code;
	public final String message;
}
