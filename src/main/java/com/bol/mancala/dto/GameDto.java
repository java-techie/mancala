package com.bol.mancala.dto;

import com.bol.mancala.enums.GameStatus;
import com.bol.mancala.model.MancalaGame;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameDto {
	private String id;
	private BoardDto board;
	private GameStatus status;

	public static GameDto fromMancalaGame(MancalaGame game) {
		return new GameDto(game.getId(), BoardDto.fromBoard(game.getBoard()), game.getStatus());
	}
}
