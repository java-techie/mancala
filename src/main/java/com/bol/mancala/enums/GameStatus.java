package com.bol.mancala.enums;

/**
 * enum constants is used for game status
 * 
 * @author anghan
 *
 */
public enum GameStatus {
	NEW // game is just initializing or just having one player
	, IN_PROGRESS // game is started with two players
	, PAUSE // future scope for starting the game from previous pause stage
	, FINISHED // game is ended
	,;
}
