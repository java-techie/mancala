package com.bol.mancala.enums;

/**
 * enum constants is used for player turn in game.
 * 
 * @author anghan
 *
 */
public enum PlayerTurn {
	FIRST_PLAYER, SECOND_PLAYER;
}
