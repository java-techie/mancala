package com.bol.mancala.enums;

/**
 * enum constants is used for winner for the game.
 * 
 * @author anghan
 *
 */
public enum GameWinner {
	FIRST_PLAYER, SECOND_PLAYER, DRAW,;
}
