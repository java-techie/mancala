package com.bol.mancala.constants;

/**
 * This class is used for all constants variable for application.
 * 
 * @author anghan
 *
 */
public class ApplicationContants {
	public static final int INITIALLY_PITS = 6;
	public static final int INITIALLY_STONE_IN_ONE_PITS = 5;

	// parameter constants
	public static final String PARAM_GAME_ID = "gameId";
	public static final String PARAM_GAME = "game";
	public static final String PARAM_TRACE_ID = "traceId";

	// aop global exception constants
	public static final String STATUS = "status";
	public static final String MESSAGE = "message";
	public static final String ERROR_VIEW = "error";
}
