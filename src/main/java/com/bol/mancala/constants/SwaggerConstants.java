package com.bol.mancala.constants;

/**
 * This class used to all swagger constants like variable, request/ response
 * sample example.
 * 
 * @author anghan
 *
 */
public class SwaggerConstants {

	public static final String CREATE_GAME_RESPONSE_EXAMPLE = "{\n"
			+ "  \"id\": \"c432439b-7f9e-4390-8717-2c41c155bf93\",\n" + "  \"board\": {\n"
			+ "    \"firstPlayerPits\": [\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n"
			+ "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n"
			+ "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      }\n" + "    ],\n"
			+ "    \"firstPlayerScore\": 0,\n" + "    \"secondPlayerPits\": [\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n"
			+ "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n"
			+ "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      }\n" + "    ],\n" + "    \"secondPlayerScore\": 0,\n"
			+ "    \"playerTurn\": \"FIRST_PLAYER\",\n" + "    \"result\": null\n" + "  },\n"
			+ "  \"status\": \"IN_PROGRESS\"\n" + "}";

	public static final String RESET_GAME_RESPONSE_EXAMPLE = "{\n"
			+ "  \"id\": \"78e0789a-fae0-4a9b-bc3d-988a467478e3\",\n" + "  \"board\": {\n"
			+ "    \"firstPlayerPits\": [\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n"
			+ "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n"
			+ "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      }\n" + "    ],\n"
			+ "    \"firstPlayerScore\": 0,\n" + "    \"secondPlayerPits\": [\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n"
			+ "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n"
			+ "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      }\n" + "    ],\n" + "    \"secondPlayerScore\": 0,\n"
			+ "    \"playerTurn\": \"FIRST_PLAYER\",\n" + "    \"result\": null\n" + "  },\n"
			+ "  \"status\": \"IN_PROGRESS\",\n" + "  \"firstPlayerId\": \"df67b96d-1607-4ce9-a6bd-9b43ab6eafb5\",\n"
			+ "  \"secondPlayerId\": \"97548bb2-97ef-4cd2-b216-771b0e04f4b6\"\n" + "}";

	public static final String DELETE_GAME_RESPONSE_EXAMPLE = "deleted successfully.";

	public static final String GET_GAME_RESPONSE_EXAMPLE = "{\n"
			+ "  \"id\": \"78e0789a-fae0-4a9b-bc3d-988a467478e3\",\n" + "  \"board\": {\n"
			+ "    \"firstPlayerPits\": [\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n"
			+ "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n"
			+ "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      }\n" + "    ],\n"
			+ "    \"firstPlayerScore\": 0,\n" + "    \"secondPlayerPits\": [\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n"
			+ "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n"
			+ "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      }\n" + "    ],\n" + "    \"secondPlayerScore\": 0,\n"
			+ "    \"playerTurn\": \"FIRST_PLAYER\",\n" + "    \"result\": null\n" + "  },\n"
			+ "  \"status\": \"IN_PROGRESS\",\n" + "  \"firstPlayerId\": \"df67b96d-1607-4ce9-a6bd-9b43ab6eafb5\",\n"
			+ "  \"secondPlayerId\": \"97548bb2-97ef-4cd2-b216-771b0e04f4b6\"\n" + "}\n" + "";

	public static final String MAKE_MOVE_RESPONSE_EXAMPLE = "{\n"
			+ "  \"id\": \"78e0789a-fae0-4a9b-bc3d-988a467478e3\",\n" + "  \"board\": {\n"
			+ "    \"firstPlayerPits\": [\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 0\n" + "      },\n"
			+ "      {\n" + "        \"stone\": 0\n" + "      },\n" + "      {\n" + "        \"stone\": 6\n"
			+ "      },\n" + "      {\n" + "        \"stone\": 6\n" + "      }\n" + "    ],\n"
			+ "    \"firstPlayerScore\": 2,\n" + "    \"secondPlayerPits\": [\n" + "      {\n"
			+ "        \"stone\": 5\n" + "      },\n" + "      {\n" + "        \"stone\": 5\n" + "      },\n"
			+ "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n" + "        \"stone\": 4\n"
			+ "      },\n" + "      {\n" + "        \"stone\": 4\n" + "      },\n" + "      {\n"
			+ "        \"stone\": 4\n" + "      }\n" + "    ],\n" + "    \"secondPlayerScore\": 0,\n"
			+ "    \"playerTurn\": \"SECOND_PLAYER\",\n" + "    \"result\": null\n" + "  },\n"
			+ "  \"status\": \"IN_PROGRESS\",\n" + "  \"firstPlayerId\": \"df67b96d-1607-4ce9-a6bd-9b43ab6eafb5\",\n"
			+ "  \"secondPlayerId\": \"97548bb2-97ef-4cd2-b216-771b0e04f4b6\"\n" + "}";
}
