package com.bol.mancala.constants;

import lombok.Getter;

/**
 * Class is used to define application error code and description.
 * 
 * @author anghan
 *
 */
@Getter
public enum ErrorCodeMessages {

	MANCALA_GAME_NOT_ABLE_DELETE("MANCALA_GAME_NOT_ABLE_DELETE",
			"May be game not found in DB or some other reason not able to delete."),;

	String errorCode;

	String errorMessage;

	private ErrorCodeMessages(String errCode, String errMessage) {
		this.errorCode = errCode;
		this.errorMessage = errMessage;
	}

}
