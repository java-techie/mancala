package com.bol.mancala.utils;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Class is used to take lock
 * 
 * @author anghan
 *
 * @param <T>
 */
public class ExclusiveLockByKey<T> {

	private static Set usedKeys = ConcurrentHashMap.newKeySet();

	/**
	 * It will return true if key not present else false.
	 * 
	 * @param key
	 * @return
	 */
	public boolean tryLock(T key) {
		return usedKeys.add(key);
	}

	/**
	 * release the lock base on key
	 * 
	 * @param key
	 */
	public void unlock(T key) {
		usedKeys.remove(key);
	}
}
