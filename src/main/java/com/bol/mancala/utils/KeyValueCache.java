package com.bol.mancala.utils;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Generic implementation of cache
 * 
 * @author anghan
 *
 * @param <K>
 * @param <V>
 */
public class KeyValueCache<K, V> {
	private Map<K, V> concurrentHashMap = new ConcurrentHashMap<K, V>();

	/**
	 * add element in cache
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public V put(K key, V value) {
		concurrentHashMap.put(key, value);
		return value;
	}

	/**
	 * get element from cache
	 *
	 * @param key V
	 * @return
	 */
	public V get(K key) {
		return concurrentHashMap.get(key);
	}

	/**
	 * remove element in cache if key not exist then return null else it will return
	 * value
	 *
	 * @param key
	 * @param value
	 * @return
	 */
	public V removeElement(K key) {
		V value = null;
		if (concurrentHashMap.get(key) != null) {
			value = concurrentHashMap.remove(key);
		}
		return value;
	}

	/**
	 * clear the cache
	 */
	public void removeAll() {
		Set<Map.Entry<K, V>> entries = concurrentHashMap.entrySet();
		entries.forEach(entry -> removeElement(entry.getKey()));
	}

}
