package com.bol.mancala.utils;

import java.util.UUID;

import org.slf4j.MDC;

import com.bol.mancala.constants.ApplicationContants;
import com.bol.mancala.enums.PlayerTurn;

public class Utils {
	public static PlayerTurn playerTurnChange(PlayerTurn playerTurn) {
		if (playerTurn.equals(PlayerTurn.FIRST_PLAYER))
			return PlayerTurn.SECOND_PLAYER;
		else
			return PlayerTurn.FIRST_PLAYER;

	}

	public static void generateTraceId() {
		MDC.put(ApplicationContants.PARAM_TRACE_ID, UUID.randomUUID().toString());
	}
}
