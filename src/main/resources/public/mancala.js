function createGame() {

	$.ajax
		({
			type: "POST",
			url: "http://localhost:8080/mancala/game/create",
			success: function(response) {

				document.getElementById("main-wraper").classList.remove("d-none");
				document.getElementById("id").value = response.id;
				document.getElementById("status").value = response.status;
				document.getElementById("firstPlayerId").value = response.board.firstPlayerId;
				document.getElementById("secondPlayerId").value = response.board.secondPlayerId;
				document.getElementById("reset-button").classList.remove("d-none");
				document.getElementById(
						response.board.playerTurn === "FIRST_PLAYER"
							? "pl1-color"
							: "pl2-color"
					).classList.add("blink");
				document.getElementById(response.board.playerTurn === "FIRST_PLAYER"
							? "pl2-play-area"
							: "pl1-play-area"
					).classList.add("d-disabled");
				document.getElementById(response.board.playerTurn === "FIRST_PLAYER" 
							? "pl2-card" 
							: "pl2-card"
					).classList.add("d-disabled");
				document.getElementById(response.board.playerTurn === "FIRST_PLAYER"
							? "pl2-divider"
							: "pl2-divider"
					).classList.add("d-disabled");

				document.getElementById("firstPlayerScore").innerHTML =
					response.board.firstPlayerScore;
				document.getElementById("secondPlayerScore").innerHTML =
					response.board.secondPlayerScore;

				[0, 1, 2, 3, 4, 5].map((pit, index) => {
					document.getElementById(`pl1-stone-${pit}`).innerHTML =
						response.board.firstPlayerPits[pit].stone;
					document.getElementById(`pl2-stone-${pit}`).innerHTML =
						response.board.secondPlayerPits[pit].stone;
				});
			}
		});
}

function makeMove(player, pitIndx) {
	const id = document.getElementById("id").value;
	
	const firstPlayerId = document.getElementById("firstPlayerId").value;
	const secondPlayerId = document.getElementById("secondPlayerId").value;

	const url = "http://localhost:8080/mancala/game/".concat(id, "/makeMove");
	
	$.ajax
		({
			type: "POST",
			url: url,
			data: {pitIndex: pitIndx, playerId: player===1 ? firstPlayerId : secondPlayerId},
			success: function(response) {
				if (response.board.result !== null) {
					document.getElementById("main-wraper").classList.add("d-disabled");
					document.getElementById("result").classList.remove("d-none");
					switch (response.board.result) {
						case "FIRST_PLAYER":
							document.getElementById("result").innerHTML =
								"First player win game";
							break;
						case "SECOND_PLAYER":
							document.getElementById("result").innerHTML =
								"Second player win game";
							break;
						case "DRAW":
						default:
							document.getElementById("result").innerHTML = "Game draw";
					}
				}

				document.getElementById(response.board.playerTurn === "FIRST_PLAYER"
							? "pl1-color"
							: "pl2-color"
					).classList.add("blink");
					
				document.getElementById(response.board.playerTurn === "FIRST_PLAYER"
							? "pl2-play-area"
							: "pl1-play-area"
					).classList.add("d-disabled");
					
				document.getElementById(
						response.board.playerTurn === "FIRST_PLAYER" ? "pl2-card" : "pl1-card"
					).classList.add("d-disabled");
					
				document.getElementById(response.board.playerTurn === "FIRST_PLAYER"
							? "pl2-divider"
							: "pl1-divider"
					).classList.add("d-disabled");

				document.getElementById(response.board.playerTurn !== "FIRST_PLAYER"
							? "pl1-color"
							: "pl2-color"
					).classList.remove("blink");
					
				document.getElementById(response.board.playerTurn !== "FIRST_PLAYER"
							? "pl2-play-area"
							: "pl1-play-area"
					).classList.remove("d-disabled");
					
				document.getElementById(response.board.playerTurn !== "FIRST_PLAYER" 
							? "pl2-card" 
							: "pl1-card"
					).classList.remove("d-disabled");

				document.getElementById(response.board.playerTurn !== "FIRST_PLAYER"
							? "pl2-divider"
							: "pl1-divider"
					).classList.remove("d-disabled");

				document.getElementById("firstPlayerScore").innerHTML = response.board.firstPlayerScore;
				document.getElementById("secondPlayerScore").innerHTML = response.board.secondPlayerScore;

				[0, 1, 2, 3, 4, 5].map((pit, index) => {
					document.getElementById(`pl1-stone-${pit}`).innerHTML =
						response.board.firstPlayerPits[pit].stone;
					document.getElementById(`pl2-stone-${pit}`).innerHTML =
						response.board.secondPlayerPits[pit].stone;
				});
			}
		});
}

function resetGame() {
	const id = document.getElementById("id").value;
	$.ajax
	({
		type: "PUT",
		url: "http://localhost:8080/mancala/game/" + id + "/reset",
		success: function(response) {
			document.getElementById("main-wraper").classList.remove("d-none");
			document.getElementById("status").value = response.status;
			document.getElementById("firstPlayerId").value = response.board.firstPlayerId;
			document.getElementById("secondPlayerId").value = response.board.secondPlayerId;
			document.getElementById("reset-button").classList.remove("d-none");
			document.getElementById(response.board.playerTurn === "FIRST_PLAYER"
						? "pl1-color"
						: "pl2-color"
				).classList.add("blink");
			document.getElementById(response.board.playerTurn === "FIRST_PLAYER"
						? "pl2-play-area"
						: "pl1-play-area"
				).classList.add("d-disabled");
			document.getElementById(response.board.playerTurn === "FIRST_PLAYER" 
						? "pl2-card" 
						: "pl2-card"
				).classList.add("d-disabled");
			document.getElementById(response.board.playerTurn === "FIRST_PLAYER"
						? "pl2-divider"
						: "pl2-divider"
				).classList.add("d-disabled");
			document.getElementById(response.board.playerTurn !== "FIRST_PLAYER"
						? "pl1-color"
						: "pl2-color"
				).classList.remove("blink");
			document.getElementById(response.board.playerTurn !== "FIRST_PLAYER"
						? "pl2-play-area"
						: "pl1-play-area"
				).classList.remove("d-disabled");
				
			document.getElementById(response.board.playerTurn !== "FIRST_PLAYER" 
						? "pl2-card" 
						: "pl1-card")
				.classList.remove("d-disabled");
			document.getElementById(response.board.playerTurn !== "FIRST_PLAYER"
						? "pl2-divider"
						: "pl1-divider"
				).classList.remove("d-disabled");
		
			document.getElementById("firstPlayerScore").innerHTML =
				response.board.firstPlayerScore;
			document.getElementById("secondPlayerScore").innerHTML =
				response.board.secondPlayerScore;
		
			[0, 1, 2, 3, 4, 5].map((pit, index) => {
				document.getElementById(`pl1-stone-${pit}`).innerHTML =
					response.board.firstPlayerPits[pit].stone;
				document.getElementById(`pl2-stone-${pit}`).innerHTML =
					response.board.secondPlayerPits[pit].stone;
			});
		}
	});		
}