package com.bol.mancala;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.bol.mancala.enums.GameWinner;
import com.bol.mancala.enums.PlayerTurn;
import com.bol.mancala.model.Board;
import com.bol.mancala.utils.TestUtils;

@SpringBootTest
public class BoardModelTests {

	@Test
	public void normalMoveTest() {
		Board board = TestUtils.generateBoard(Lists.newArrayList(4, 4, 4, 4, 4, 4), 0,
				Lists.newArrayList(4, 4, 4, 4, 4, 4), 0, PlayerTurn.FIRST_PLAYER);
		board.makeMove(0);
		assertTrue(board.getFirstPlayer().getPits().get(0).getStone() == 0); // same score and next turn
		assertTrue(board.getFirstPlayer().getPits().get(1).getStone() == 5);
	}

	@Test
	public void repeatPlayerTurnTest() {
		Board board = TestUtils.generateBoard(Lists.newArrayList(0, 0, 0, 0, 1, 1), 6,
				Lists.newArrayList(0, 0, 0, 0, 3, 1), 1, PlayerTurn.FIRST_PLAYER);
		board.makeMove(5);
		assertTrue(board.getPlayerTurn().equals(PlayerTurn.FIRST_PLAYER));
	}

	@Test
	public void nextPlayerTurnTest() {
		Board board = TestUtils.generateBoard(Lists.newArrayList(0, 0, 0, 0, 1, 0), 6,
				Lists.newArrayList(0, 0, 0, 0, 3, 2), 1, PlayerTurn.FIRST_PLAYER);
		board.makeMove(4);
		assertTrue(board.getPlayerTurn().equals(PlayerTurn.SECOND_PLAYER));
	}

	@Test
	public void checkFirstWinnerTest() {
		Board board = TestUtils.generateBoard(Lists.newArrayList(0, 0, 0, 0, 0, 1), 6,
				Lists.newArrayList(0, 0, 0, 0, 3, 1), 1, PlayerTurn.FIRST_PLAYER);
		board.makeMove(5);
		assertTrue(board.getResult().equals(GameWinner.FIRST_PLAYER));
	}

	@Test
	public void checkSecondWinnerTest() {
		Board board = TestUtils.generateBoard(Lists.newArrayList(0, 0, 0, 0, 0, 1), 1,
				Lists.newArrayList(0, 0, 0, 0, 3, 1), 6, PlayerTurn.FIRST_PLAYER);
		board.makeMove(5);
		assertTrue(board.getResult().equals(GameWinner.SECOND_PLAYER));
	}

	@Test
	public void drawGameTest() {
		Board board = TestUtils.generateBoard(Lists.newArrayList(0, 0, 0, 0, 0, 1), 5,
				Lists.newArrayList(0, 0, 0, 0, 3, 1), 2, PlayerTurn.FIRST_PLAYER);
		board.makeMove(5);
		assertTrue(board.getResult().equals(GameWinner.DRAW));
	}

	@Test
	public void catchOpponentStoneTest() {
		Board board = TestUtils.generateBoard(Lists.newArrayList(0, 0, 0, 1, 1, 0), 4,
				Lists.newArrayList(3, 0, 0, 0, 0, 1), 2, PlayerTurn.FIRST_PLAYER);
		board.makeMove(4);
		assertTrue(board.getFirstPlayer().getScore() == 8);
	}
}