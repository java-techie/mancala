package com.bol.mancala.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import com.bol.mancala.enums.PlayerTurn;
import com.bol.mancala.model.Board;
import com.bol.mancala.model.Pit;
import com.bol.mancala.model.Player;

public class TestUtils {
	public static Board generateBoard(List<Integer> firstPlayerPitStone, int firstPlayerScore,
			List<Integer> secondPlayerPitStone, int secondPlayerScore, PlayerTurn playerTurn) {
		List<Pit> firstPlayerPits = new ArrayList<>();
		firstPlayerPits.add(new Pit(firstPlayerPitStone.get(0)));
		firstPlayerPits.add(new Pit(firstPlayerPitStone.get(1)));
		firstPlayerPits.add(new Pit(firstPlayerPitStone.get(2)));
		firstPlayerPits.add(new Pit(firstPlayerPitStone.get(3)));
		firstPlayerPits.add(new Pit(firstPlayerPitStone.get(4)));
		firstPlayerPits.add(new Pit(firstPlayerPitStone.get(5)));
		Player firstPlayerSide = new Player(UUID.randomUUID().toString(), firstPlayerPits, firstPlayerScore);

		List<Pit> secondPlayerPits = new ArrayList<>();
		secondPlayerPits.add(new Pit(secondPlayerPitStone.get(0)));
		secondPlayerPits.add(new Pit(secondPlayerPitStone.get(1)));
		secondPlayerPits.add(new Pit(secondPlayerPitStone.get(2)));
		secondPlayerPits.add(new Pit(secondPlayerPitStone.get(3)));
		secondPlayerPits.add(new Pit(secondPlayerPitStone.get(4)));
		secondPlayerPits.add(new Pit(secondPlayerPitStone.get(5)));
		Player secondPlayerSide = new Player(UUID.randomUUID().toString(), secondPlayerPits, secondPlayerScore);

		return new Board(firstPlayerSide, secondPlayerSide, playerTurn, null);
	}
}
