package com.bol.mancala;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.bol.mancala.constants.ApplicationContants;
import com.bol.mancala.dto.GameDto;
import com.bol.mancala.enums.PlayerTurn;
import com.bol.mancala.exception.GameLogicException;
import com.bol.mancala.exception.GameNotFoundException;
import com.bol.mancala.service.GameService;

@SpringBootTest
class GameServiceTests {

	@Autowired
	GameService gameService;

	@Test
	void createGameTest() {
		GameDto gameDto = gameService.createGame();
		assertNotNull(gameDto.getId());
		assertNotNull(gameDto.getBoard().getFirstPlayerId());
		assertNotNull(gameDto.getBoard().getSecondPlayerId());
	}

	@Test
	void loadGameTest() {
		GameDto gameDto = gameService.createGame();
		GameDto loadedGameDto = gameService.loadGame(gameDto.getId());
		assertEquals(gameDto.getId(), loadedGameDto.getId());
		assertEquals(gameDto.getBoard().getFirstPlayerId(), loadedGameDto.getBoard().getFirstPlayerId());
		assertEquals(gameDto.getBoard().getSecondPlayerId(), loadedGameDto.getBoard().getSecondPlayerId());
	}

	@Test
	void deleteGameTest() {
		GameDto gameDto = gameService.createGame();
		assertTrue(gameService.deleteGame(gameDto.getId()));
	}

	@Test
	void makeMoveWithAgainTurnTest() {
		GameDto gameDto = gameService.createGame();
		PlayerTurn playerTurn = gameDto.getBoard().getPlayerTurn();
		gameService.makeMove(gameDto.getId(), gameDto.getBoard().getFirstPlayerId(),
				ApplicationContants.INITIALLY_PITS - ApplicationContants.INITIALLY_STONE_IN_ONE_PITS);
		GameDto loadedGameDto = gameService.loadGame(gameDto.getId());
		assertTrue(playerTurn.equals(loadedGameDto.getBoard().getPlayerTurn()));
	}

	@Test
	void makeMoveWithNextPlayerTurnTest() {
		GameDto gameDto = gameService.createGame();
		PlayerTurn playerTurn = gameDto.getBoard().getPlayerTurn();
		gameService.makeMove(gameDto.getId(), gameDto.getBoard().getFirstPlayerId(),
				ApplicationContants.INITIALLY_PITS - ApplicationContants.INITIALLY_STONE_IN_ONE_PITS);
		GameDto loadedGameDto = gameService.loadGame(gameDto.getId());
		assertTrue(playerTurn.equals(loadedGameDto.getBoard().getPlayerTurn()));
	}

	@Test
	void notValidGameTest() {
		GameDto gameDto = gameService.createGame();
		assertThrows(GameNotFoundException.class,
				() -> gameService.makeMove("dummyGameId", gameDto.getBoard().getFirstPlayerId(), 1));
	}

	@Test
	void notValidTurnTest() {
		GameDto gameDto = gameService.createGame();
		assertThrows(GameLogicException.class, () -> gameService.makeMove(gameDto.getId(),
				gameDto.getBoard().getSecondPlayerId(), ApplicationContants.INITIALLY_PITS - 1));
	}
}